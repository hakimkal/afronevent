from django.contrib import admin

# Register your models here.
from django.contrib import admin
# from image_cropping import ImageCroppingMixin
from gallery.models import Gallery

class GalleryAdmin(admin.ModelAdmin):
    #exclude = ('slider_position',)
    list_display = ('title','photos','created','service' )
    class Meta:
        model = Gallery


admin.site.register(Gallery, GalleryAdmin)