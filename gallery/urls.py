from django.conf.urls import url
from gallery.views import GalleryList


urlpatterns = [
    url(r'(?P<service>[\w-]+|)/$', GalleryList.as_view(), name="filterByService"),
    url(r'^$', GalleryList.as_view(),name= "gallery")
               ]