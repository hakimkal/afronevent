from django.db import models

from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import ResizeToFill, ResizeToFit,Adjust
from afronevents.settings import ENV_PATH, BASE_DIR

from datetime import datetime

from events.models import Event
from services.models import Service


class Gallery(models.Model):






    photos = models.ImageField(upload_to='service_photo_thumbs', null=True, blank=True)
    thumbnail = ImageSpecField([Adjust(sharpness=1.1),
                                ResizeToFit(257, 247)], source='photos',
                               format='JPEG', options={'quality': 100})

    title = models.CharField(max_length=255, blank=True, null=True)
    service = models.ForeignKey(Service, null=True, blank=True)
    event = models.ForeignKey(Event, null=True, blank=True)
    description = models.TextField(blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title
    #code
