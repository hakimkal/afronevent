from django.shortcuts import render
from django.views.generic import ListView

from services.models import Service
from .models import Gallery


class GalleryList(ListView):
    model = Gallery

    template_name = "gallery/gallery.html"
    paginate_by = 28

    def get_queryset(self):
        queryset = super(GalleryList, self).get_queryset()
        service = self.kwargs.get("service")

        if service is not None:
            queryset = queryset.filter(service__slug=service).order_by('-created')

        return queryset

    def get_context_data(self, **kwargs):
        ctx = super(GalleryList,self).get_context_data(**kwargs)
        ctx["services"] = Service.objects.all()
        return ctx
