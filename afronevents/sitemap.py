from django.contrib.sitemaps import Sitemap
import datetime
from django.core.urlresolvers import reverse

class AfroneventsSitemap(Sitemap):


    def __init__(self,names):
        self.names = names

    def items(self):
        return self.names

    def changefreq(self, obj):
        return 'weekly'

    def lastmod(self, obj):
        return datetime.datetime.now()

    def location(self, obj):
        return reverse('pages:page',kwargs={'slug':obj})




from publicpages.models import PublicPage

class PublicPageSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return PublicPage.objects.filter(publish=True)

    def lastmod(self, obj):
        return obj.modified

    def location(self, obj):
        return reverse('pages:page',kwargs={'slug':obj.slug})




from events.models import Event
class EventSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Event.objects.filter(publish=True)

    def lastmod(self, obj):
        return obj.modified

    def location(self, obj):
        return reverse('events:event',kwargs={'slug':obj.slug})



