from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from sitemap import *
handler404 = 'publicpages.views.custom404'

urlpatterns = [
    url(r'^manage/', admin.site.urls),
    url(r'^', include('publicpages.urls', namespace="pages")),
    #url(r'^page/',include('publicpages.urls', namespace="pages")),
    url(r'^photos/',include('gallery.urls', namespace="gallery")),
    url('^events/', include('events.urls', namespace='events')),
    url(r'^daguerre/', include('daguerre.urls')),


]

sitemaps = {
    'events': EventSitemap,
    'contact':AfroneventsSitemap(['contact']),
    'public': PublicPageSitemap
}






if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)