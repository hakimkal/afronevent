from django.conf.urls import url
from . import views

from .views import ContactUs, StaticPage, AboutUs, home, Services


urlpatterns = [
    url(r'^$', home, name='home'),
    #url(r'^$', current_datetime, name="new-home"),
    url(r'^(?P<slug>\w+|)$', StaticPage, name = "page"),
    url(r'^contact/$', ContactUs.as_view(), name= "contact"),
    url(r'^about/$', AboutUs, name="about"),
    url(r'^services/$', Services, name="services"),
    ]