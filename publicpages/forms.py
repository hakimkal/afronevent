from django import forms as form
from django.template import Context
from django.template.loader  import get_template
from django.conf import settings

from django.core.mail import EmailMessage

class ContactForm(form.Form):
    email = form.EmailField()
    name = form.CharField()
    subject = form.CharField()
    phone = form.CharField()
    message = form.CharField( widget=form.Textarea)
    
    
    def send_email(self):
        # send email using the self.cleaned_data dictionary
        email = self.cleaned_data["email"]
        subject  = self.cleaned_data["subject"]
        message = self.cleaned_data["message"]
        phone = self.cleaned_data["phone"]
        name  = self.cleaned_data["name"]
        to = ['info@afronevents.com']
       
        bcc_list = ['info@leproghrammeen.com']
        from_email = email
        ctx = {'email':email, 'message':message, 'name': name, 'subject':subject, 'phone':phone}
        message = get_template('publicpages/email.html').render(Context(ctx))
        msg = EmailMessage(subject,message, to=to, from_email="info@afronevents.com" ,bcc=bcc_list,  headers = {'Reply-To': email })
        msg.content_subtype = 'html'
        if msg.send():
            return True
        else:
            return False 
        