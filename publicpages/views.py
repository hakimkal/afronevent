import json

from django.shortcuts import render, redirect
from django.db.models import Q

from gallery.models import Gallery
from publicpages.models import PublicPage
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import DetailView, ListView, FormView
from publicpages.forms import ContactForm
from django.contrib import messages
from publicpages.forms import ContactForm
from django.template.loader import get_template
from braces.views import LoginRequiredMixin, MessageMixin
import datetime

from services.models import Service
from sliders.models import Slider
from events.models import Event

from django.shortcuts import render, redirect

from testimonials.models import Testimony


def current_datetime(request):
    # dct = {'current_date': now ,'news': news, 'sliders' : sliders, 'about' : about, 'mission' : mission, 'vision' : 'vision', 'teachers' : teachers, 'events' : events}

    return render(request, 'publicpages/home.html')


def home(request):
    now = datetime.datetime.now()
    testimonies  = Testimony.objects.filter(~Q(photos=None ))
    sliders = Slider.objects.all().order_by('-created')
    events = Event.objects.all().order_by('-created')[0:8]
    galleries = Gallery.objects.filter(created__gte= (datetime.date.today() - datetime.timedelta(days=60)))
    services = Service.objects.all()

    dct = {'current_date': now, 'sliders': sliders, 'events': events, 'services': services,'galleries':galleries,'testimonies' :testimonies}

    return render(request, 'publicpages/home.html', dct)


class StaticPage(DetailView, MessageMixin):
    model = PublicPage
    slug_url_field = 'slug'
    slug_url_kwarg = 'slug'
    template_name_field = 'html_template'

    def get_context_data(self, **kwargs):
        ctx = super(StaticPage, self).get_context_data(**kwargs)
        # ctx['management'] = Team.objects.filter(group='Management')

        return ctx

    def get_queryset(self):
        queryset = super(StaticPage, self).get_queryset()
        if self.kwargs.get('slug') != "":
            return queryset.filter(slug=self.kwargs.get('slug'))
        # .order_by('-created')



        # return reverse('pages:page',kwargs={'slug':self.slug})
        # def get_context_data(self,**kwargs):
        # ctx = super(StaticPage,self).get_context_data(**kwargs)


def AboutUs(request):
    galleries = Gallery.objects.filter(created__gte=(datetime.date.today() - datetime.timedelta(days=60)))
    services = Service.objects.all()
    dic={'galleries': galleries,'services':services}
    return render(request, 'publicpages/pages/about.html',dic)


def Services(request):
    galleries = Gallery.objects.filter(created__gte=(datetime.date.today() - datetime.timedelta(days=60)))
    dic = {'galleries': galleries}
    return render(request, 'publicpages/pages/services.html', dic)


class ContactUs(MessageMixin, FormView):
    template_name = 'publicpages/pages/contact.html'
    form_class = ContactForm
    success_url = '/'



    def form_valid(self, form):
        # form.cleaned
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        if form.send_email() == True:
            self.messages.success('Thanks for your email, we will be in touch')
            json_data = json.dumps({"success": "Thanks for your email, we will be in touch"})
            return HttpResponse(json_data, content_type="application/json")

        return super(ContactUs, self).form_valid(form)

    def form_invalid(self, form):
        print "failed form"
        print form.errors
        return HttpResponseRedirect('/')


"""

To do
"""


def get_the_nearest_event():
    pass


def get_three_upcoming_events():
    pass
