from afronevents.settings import ENV_PATH, BASE_DIR
from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from datetime import datetime
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill, ResizeToFit

from services.models import Service

BOOL_OPTS = (
        (True, 'Yes')
        ,
        (False, 'No'),

    )


class EventQuerySet(models.query.QuerySet):
    def comedy(self):
        return self.filter(comedy=True)

    def sports(self):
        return self.filter(sports=True)

    def entertainment(self):
        return self.filter(entertainment=True)

    def festival(self):
        return self.filter(festival=True)

    def outdoor(self):
        return self.filter(outdoor=True)

class EventManager(models.Manager):
    def get_queryset(self):
        return EventQuerySet(self.model, using=self._db)
    def comedy(self):
        return self.get_queryset().comedy()
    def sports(self):
        return self.get_queryset().sports()
    def entertainment(self):
        return self.get_queryset().entertainment()
    def festival(self):
        return self.get_queryset().festival()
    def outdoor(self):
        return self.get_queryset().outdoor()


class EventCategory(models.Model):


    name = models.CharField(max_length=100)

    details = models.TextField(blank=True)
    date = models.DateTimeField(default = datetime.now())

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    slug = models.SlugField(max_length=255,   unique=True)

    def __unicode__(self):
        return self.name


class Event(models.Model):


    name = models.CharField(max_length=100)
    venue = models.CharField(max_length=250)
    details = models.TextField(blank=True)
    date = models.DateTimeField(default = datetime.now())
    photo = ProcessedImageField(upload_to='events',null=True, blank=True,
                                           processors=[ResizeToFit(376, 235)],
                                           format='JPEG',
                                           options={'quality': 80})
    publish = models.BooleanField(choices=BOOL_OPTS, default = False)
    service = models.ForeignKey(Service, blank=True, null=True)
    comedy = models.BooleanField(default = False)
    sports = models.BooleanField(default = False)
    festival = models.BooleanField(default = False)
    outdoor = models.BooleanField(default = False)
    entertainment = models.BooleanField(default = False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    user  = models.ForeignKey(settings.AUTH_USER_MODEL)
    slug = models.SlugField(max_length=255,   unique=True)

    objects = EventManager()
    #code
    def __unicode__(self):
        return self.name

    def get_absolute_url(self):

        return reverse('events:event',kwargs={'slug': self.slug})


    def save_model(self, request, obj,form , change):

        if not change:

            obj.user = request.user
        obj.save()