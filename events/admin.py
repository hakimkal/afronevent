from django.db import models
from django.contrib import admin
from django.utils.text import slugify
from django.contrib.admin.options import TabularInline
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.translation import ugettext_lazy as _
import itertools
from .models import Event, EventCategory

class EventCategoryAdmin(admin.ModelAdmin):
    list_display  = ['name','details', 'created','modified']
    exclude = ('user',)
    prepopulated_fields = {"slug": ("name",)}
    formfield_overrides = {
        models.ManyToManyField: {'widget': FilteredSelectMultiple(
            verbose_name=_('events categories'),
            is_stacked=False
            )},
    }
    def save_model(self, request, obj,form , change):

        if not change:

            obj.user = request.user
        obj.save()



class EventAdmin(admin.ModelAdmin):
    list_display  = ['name', 'created','modified']
    exclude = ('user',)
    prepopulated_fields = {"slug": ("name",)}
    formfield_overrides = {
        models.ManyToManyField: {'widget': FilteredSelectMultiple(
            verbose_name=_('events'),
            is_stacked=False
            )},
    }


admin.site.register(Event, EventAdmin)
admin.site.register(EventCategory, EventCategoryAdmin)