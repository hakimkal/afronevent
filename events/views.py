from django.shortcuts import render
from django.shortcuts import render,redirect
from django.core.urlresolvers import reverse
from django.views.generic  import ListView, DetailView
from .models import Event
from datetime import datetime , timedelta  , date

class EventListView(ListView):
    model = Event
    template_name  = "events/event-grid.html"
    context_object_name = 'events'
    paginate_by  = 6

    def get_queryset(self):
        today = datetime.now()
        startdate = today + timedelta(days=-5000)

        queryset = super(EventListView, self).get_queryset()
        if self.kwargs.get('recent') != "" :
            return queryset.filter(publish=True).order_by('-created')

        else:
           return queryset.filter(publish=True).order_by('-created')

        return queryset

class EventDetailView(DetailView):
    model = Event

    template_name  = "events/single-event.html"
    events = Event.objects.all().order_by('created')[0:3]
    def get_queryset(self):


        queryset = super(EventDetailView, self).get_queryset()
        print self.kwargs.get("slug")
        return queryset.filter(slug=self.kwargs.get("slug"))

    def get_context_data(self,**kwargs):
        ctx = super(EventDetailView,self).get_context_data(**kwargs)
        today = datetime.now()
        startdate = today + timedelta(days=-16)

        ctx['events_list'] = Event.objects.filter(created__range=[startdate, today],publish=True).order_by('-created')[:3]
        return ctx

    def get(self, request,*args,**kwargs):
        if self.kwargs.get("slug") == "" or self.kwargs.get("slug") == None:
            return redirect(reverse("events:events"))
        else:
            return super(EventDetailView,self).get(request,*args,**kwargs)
