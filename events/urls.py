from django.conf.urls import url
from events.views import  EventListView, EventDetailView
urlpatterns = [
    url(r'(?P<slug>[\w-]+|)/$', EventDetailView.as_view(),name= "event"),
    url(r'^$', EventListView.as_view(), name = "events"),
    ]