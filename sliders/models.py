from django.db import models

from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import ResizeToFill, ResizeToFit, Adjust
from afronevents.settings import ENV_PATH, BASE_DIR

from datetime import datetime


class Slider(models.Model):
    CAT_OPTS = (
        ('Home', 'Home Page'),
        ('About', 'About Page'),
        ('FAQ', 'FAQs Page'),
        ('Services', 'Service page'),
        ('Event', 'Event page')

    )



    photos = models.ImageField(upload_to='sliders', null=True, blank=True)
    thumbnail = ImageSpecField([Adjust(sharpness=1.1),
                                ResizeToFill(1600, 753)], source='photos',
                               format='JPEG', options={'quality': 100})


    title = models.CharField(max_length=255, blank=True, null=True)
    private_name = models.CharField(max_length=255, blank=True, null=True,
                                    help_text="This never gets shown to the public, It serves as a marker for admin use of pictures.")
    url = models.URLField(blank=True, null=True)
    url_type = models.CharField(max_length=40, choices=(
        ('external', 'External'),
        ('internal', 'Internal'),
    ), default='internal')
    slider_position = models.IntegerField(auto_created=True, default=1000)
    description = models.TextField(blank=True, null=True)
    category = models.CharField(max_length=50, default='Home', choices=CAT_OPTS)
    created = models.DateTimeField(default=datetime.now())
    modified = models.DateTimeField(default=datetime.now())

    def __unicode__(self):
        return self.title
        # code
