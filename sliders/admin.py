from django.contrib import admin
# from image_cropping import ImageCroppingMixin
from sliders.models import Slider

class SliderAdmin(admin.ModelAdmin):
    #exclude = ('slider_position',)
    list_display = ('private_name','photos','created', 'category','modified','slider_position')
    class Meta:
        model = Slider
        

admin.site.register(Slider, SliderAdmin)