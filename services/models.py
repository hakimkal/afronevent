from django.db import models

# Create your models here.
from django.db import models

from imagekit.models import  ImageSpecField
from imagekit.processors import ResizeToFill, ResizeToFit


from imagekit.processors import resize, Adjust

from afronevents.settings import ENV_PATH, BASE_DIR

from datetime import datetime



class Service(models.Model):





    photos = models.ImageField(upload_to='service_photo_thumbs', null=True, blank=True)
    thumbnail = ImageSpecField([Adjust(sharpness=1.1),
                           ResizeToFill(257, 247)], source='photos',
                          format='JPEG', options={'quality': 100})

    title = models.CharField(max_length=255, blank=True, null=True)

    description = models.TextField(blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    slug  = models.SlugField(null=True)

    def __unicode__(self):
        return self.title
    #code
