from __future__ import unicode_literals

from django.db import models
from imagekit.models import  ImageSpecField
from imagekit.processors import ResizeToFill, ResizeToFit


from imagekit.processors import resize, Adjust

from afronevents.settings import ENV_PATH, BASE_DIR

from datetime import datetime

class Testimony(models.Model):
    photos = models.ImageField(upload_to='testimonial_pics', null=True, blank=True)
    thumbnail = ImageSpecField([Adjust(sharpness=1.1),
                                ResizeToFit(500, 500)], source='photos',
                               format='JPEG', options={'quality': 100})

    customer = models.CharField(max_length=255, blank=True, null=True)


    remarks = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.customer
        # code
