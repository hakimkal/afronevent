from django.contrib import admin

from testimonials.models import Testimony


class TestiomonialAdmin(admin.ModelAdmin):
    model = Testimony
    list_display = ['customer', 'remarks']


admin.site.register(Testimony, TestiomonialAdmin)